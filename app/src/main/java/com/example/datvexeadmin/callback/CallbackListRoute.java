package com.example.datvexeadmin.callback;

import com.example.datvexeadmin.Model.ListRouteModel;

import java.util.List;

public interface CallbackListRoute {
    void OnListRouteLoadSuccess (List<ListRouteModel> listRouteModels);
    void OnListRouteLoadFailed(String message);
}
