package com.example.datvexeadmin.callback;

import com.example.datvexeadmin.Model.ListBusModel;

import java.util.List;

public interface CallbackListBus {
    void OnListBusLoadSuccess (List<ListBusModel> listBusModels);
    void OnListBusLoadFailed(String message);
}
