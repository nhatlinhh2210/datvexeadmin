package com.example.datvexeadmin.callback;

import android.view.View;

public interface RecyclerClickListener {
    void OnItemClickListener (View view, int pos);
}
