package com.example.datvexeadmin.ui.home;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.datvexeadmin.Model.ListRouteModel;
import com.example.datvexeadmin.R;
import com.example.datvexeadmin.ui.SignupActitity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddrouteFragment extends Fragment {

    EditText et_routeName, et_routeStart, et_routeEnd, et_routeSerial, et_routeSeat, et_routeSeatType, et_routeTimeStart, et_routeTimeEnd, et_routeDayStart, et_routeDayEnd, et_routeDistance, et_routePrice;
    Button btn_addRoute;
    NavController navController;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_addroute, container, false);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference routeRef = firebaseDatabase.getReference("Route");
        navController = Navigation.findNavController(view);

        et_routeName = view.findViewById(R.id.et_routeName);
        et_routeStart = view.findViewById(R.id.et_routeStart);
        et_routeEnd = view.findViewById(R.id.et_routeEnd);
        et_routeSerial = view.findViewById(R.id.et_routeSerial);
        et_routeSeat = view.findViewById(R.id.et_routeSeat);
        et_routeSeatType = view.findViewById(R.id.et_routeSeatType);
        et_routeTimeStart = view.findViewById(R.id.et_routeTimeStart);
        et_routeTimeEnd = view.findViewById(R.id.et_routeTimeEnd);
        et_routeDayStart = view.findViewById(R.id.et_routeDayStart);
        et_routeDayEnd = view.findViewById(R.id.et_routeDayEnd);
        et_routeDistance = view.findViewById(R.id.et_routeDistance);
        et_routePrice = view.findViewById(R.id.et_routePrice);

        btn_addRoute = view.findViewById(R.id.btn_addRoute);

        btn_addRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog mDialog = new ProgressDialog(getActivity());
                mDialog.setMessage("Đang xử lý...");
                mDialog.show();
                routeRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.child(et_routeSerial.getText().toString()).exists()) {
                            mDialog.dismiss();
                            Toast.makeText(getActivity(), "Mã tuyến đã tồn tại", Toast.LENGTH_SHORT).show();
                        } else {
                            mDialog.dismiss();
                            ListRouteModel routeModel1 = new ListRouteModel(
                                    et_routeName.getText().toString(),
                                    et_routeSerial.getText().toString(),
                                    et_routeStart.getText().toString(),
                                    et_routeEnd.getText().toString(),
                                    et_routeSeatType.getText().toString(),
                                    et_routeTimeStart.getText().toString(),
                                    et_routeTimeEnd.getText().toString(),
                                    et_routeDayStart.getText().toString(),
                                    et_routeDayEnd.getText().toString(),
                                    et_routePrice.getText().toString(),
                                    et_routeSeat.getText().toString(),
                                    et_routeDistance.getText().toString());
                            routeRef.child(et_routeSerial.getText().toString()).setValue(routeModel1);
                            Toast.makeText(getActivity(), "Đã thêm thành công", Toast.LENGTH_SHORT).show();
                            navController.navigate(R.id.action_nav_add_route_to_nav_home);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

    }
}
