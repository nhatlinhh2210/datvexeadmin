package com.example.datvexeadmin.ui.home;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.datvexeadmin.Common.Common;
import com.example.datvexeadmin.Model.ListRouteModel;
import com.example.datvexeadmin.callback.CallbackListRoute;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HomeViewModel extends ViewModel implements CallbackListRoute {

    private MutableLiveData<List<ListRouteModel>> routeList;
    private MutableLiveData<String> errorMessage;
    private CallbackListRoute callbackListRoute;

    public HomeViewModel() {
        callbackListRoute = this;
    }

    public MutableLiveData<List<ListRouteModel>> getRouteList(){
        if (routeList == null) {
            routeList = new MutableLiveData<>();
            errorMessage = new MutableLiveData<>();
            loadRouteList();
        }
        return routeList;
    }
    public MutableLiveData<String> getErrorMessage(){
        return getErrorMessage();
    }

    private void loadRouteList() {
        List<ListRouteModel> templist = new ArrayList<>();
        DatabaseReference routeRef = FirebaseDatabase.getInstance().getReference(Common.LIST_ROUTE_REF);
        routeRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot routeSnapShot:dataSnapshot.getChildren()){
                    ListRouteModel model = routeSnapShot.getValue(ListRouteModel.class);
                    templist.add(model);                }
                callbackListRoute.OnListRouteLoadSuccess(templist);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callbackListRoute.OnListRouteLoadFailed(databaseError.getMessage());
            }
        });
    }



    @Override
    public void OnListRouteLoadSuccess(List<ListRouteModel> listRouteModels) {
        routeList.setValue(listRouteModels);
    }

    @Override
    public void OnListRouteLoadFailed(String message) {
        errorMessage.setValue(message);
    }
}