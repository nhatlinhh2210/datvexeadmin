package com.example.datvexeadmin.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.example.datvexeadmin.Common.Common;
import com.example.datvexeadmin.Model.ListRouteModel;
import com.example.datvexeadmin.R;
import com.example.datvexeadmin.adapter.ListRouteAdapter;
import com.example.datvexeadmin.callback.ButtonClickListener;
import com.example.datvexeadmin.callback.RecyclerClickListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeFragment extends Fragment {

    List<ListRouteModel> routeModelList;
    ListRouteAdapter adapter;

    NavController navController;

    Unbinder unbinder;
    Button btn_addRoute, btn_routeDelete, btn_routeEdit;

    RecyclerClickListener listener;
    RecyclerView recycler_routeShowup;

    private HomeViewModel homeViewModel;

    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    DatabaseReference routeRef = firebaseDatabase.getReference("Route");

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        recycler_routeShowup = (RecyclerView) root.findViewById(R.id.recycler_routeShowup);
        init();
        homeViewModel.getRouteList().observe(this, listRouteModels -> {
            routeModelList = listRouteModels;
            adapter = new ListRouteAdapter(getContext(), routeModelList);
            ListRouteAdapter listRouteAdapter = new ListRouteAdapter(getContext(), listRouteModels);
            recycler_routeShowup.setAdapter(listRouteAdapter);
        });
        return root;
    }

    private void init() {
        recycler_routeShowup.setHasFixedSize(true);
        recycler_routeShowup.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        navController = Navigation.findNavController(view);

        btn_addRoute = (Button) view.findViewById(R.id.btn_addRoute);


        btn_addRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_nav_home_to_nav_add_route);
            }
        });
    }
}
