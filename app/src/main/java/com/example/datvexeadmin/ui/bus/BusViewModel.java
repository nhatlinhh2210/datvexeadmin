package com.example.datvexeadmin.ui.bus;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.datvexeadmin.Common.Common;
import com.example.datvexeadmin.Model.ListBusModel;
import com.example.datvexeadmin.callback.CallbackListBus;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class BusViewModel extends ViewModel implements CallbackListBus {
    private MutableLiveData<List<ListBusModel>> busList;
    private MutableLiveData<String> errorMessage;
    private CallbackListBus callbackListBus;

    public BusViewModel() {
        callbackListBus = this;
    }

    public MutableLiveData<List<ListBusModel>> getBusList() {
        if (busList == null){
            busList = new MutableLiveData<>();
            errorMessage = new MutableLiveData<>();
            loadListBus();
        }
        return busList;
    }

    private void loadListBus() {
        List<ListBusModel> templist = new ArrayList<>();
        DatabaseReference listbusRef = FirebaseDatabase.getInstance().getReference(Common.LIST_BUS_REF);
        listbusRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot itemSnapShot:dataSnapshot.getChildren()){
                    ListBusModel model = itemSnapShot.getValue(ListBusModel.class);
                    templist.add(model);
                }
                callbackListBus.OnListBusLoadSuccess(templist);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callbackListBus.OnListBusLoadFailed(databaseError.getMessage());

            }
        });
    }

    public MutableLiveData<String> getErrorMessage() {
        return errorMessage;
    }

    @Override
    public void OnListBusLoadSuccess(List<ListBusModel> listBusModels) {
        busList.setValue(listBusModels);
    }

    @Override
    public void OnListBusLoadFailed(String message) {
        errorMessage.setValue(message);
    }
}