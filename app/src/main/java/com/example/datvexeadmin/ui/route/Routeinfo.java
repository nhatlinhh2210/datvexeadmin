package com.example.datvexeadmin.ui.route;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.datvexeadmin.Model.ListRouteModel;
import com.example.datvexeadmin.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Routeinfo extends Fragment {

    private RouteinfoViewModel routeinfoViewModel;
    
    Unbinder unbinder;
    
    @BindView(R.id.tv_routeName)
    TextView tv_routeName;

    @BindView(R.id.tv_routeSerial)
    TextView tv_routeSerial;

    @BindView(R.id.tv_routeStart)
    TextView tv_routeStart;

    @BindView(R.id.tv_routeEnd)
    TextView tv_routeEnd;

    @BindView(R.id.tv_seatNumber)
    TextView tv_seatNumber;

    @BindView(R.id.tv_seatType)
    TextView tv_seatType;

    @BindView(R.id.tv_routeTimeStart)
    TextView tv_timeStart;

    @BindView(R.id.tv_routeTimeEnd)
    TextView tv_timeEnd;

    @BindView(R.id.tv_routeDayStart)
    TextView tv_routeDayStart;

    @BindView(R.id.tv_routeDayEnd)
    TextView tv_routeDayEnd;

    @BindView(R.id.tv_routeDistance)
    TextView tv_routeDistance;
    
    @BindView(R.id.tv_routePrice)
    TextView tv_routePrice;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        routeinfoViewModel =
                ViewModelProviders.of(this).get(RouteinfoViewModel.class);
        View root = inflater.inflate(R.layout.routeinfo_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);

        routeinfoViewModel.getMutableLiveDataRoute().observe(this,listRouteModel -> {
            displayInfo(listRouteModel);
        });
        return root;
    }

    private void displayInfo(ListRouteModel listRouteModel) {
        tv_routeName.setText(new StringBuilder(listRouteModel.getName()));
        tv_routeSerial.setText(new StringBuilder(listRouteModel.getSerial()));
        tv_routeStart.setText(new StringBuilder(listRouteModel.getStart()));
        tv_routeEnd.setText(new StringBuilder(listRouteModel.getEnd()));
        tv_seatNumber.setText(new StringBuilder(listRouteModel.getSeat().toString()));
        tv_seatType.setText(new StringBuilder(listRouteModel.getSeatType()));
        tv_timeStart.setText(new StringBuilder(listRouteModel.getTimeStart()));
        tv_timeEnd.setText(new StringBuilder(listRouteModel.getTimeEnd()));
        tv_routeDayStart.setText(new StringBuilder(listRouteModel.getDayStart()));
        tv_routeDayEnd.setText(new StringBuilder(listRouteModel.getDayEnd()));
        tv_routeDistance.setText(new StringBuilder(listRouteModel.getDistance().toString()));
        tv_routePrice.setText(new StringBuilder(listRouteModel.getPrice().toString()));
    }

}
