package com.example.datvexeadmin.ui.bus;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datvexeadmin.R;
import com.example.datvexeadmin.adapter.ListBusAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BusFragment extends Fragment {

    private BusViewModel busViewModel;
    NavController navController;

    Button btn_addBus;

    Unbinder unbinder;

    @BindView(R.id.recycler_busLookup)
    RecyclerView recycler_listbus;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        busViewModel =
                ViewModelProviders.of(this).get(BusViewModel.class);
        View root = inflater.inflate(R.layout.fragment_bus, container, false);

        unbinder = ButterKnife.bind(this, root);
        init();
        busViewModel.getBusList().observe(this,listBusModels -> {
            ListBusAdapter adapter = new ListBusAdapter(getContext(),listBusModels);
            recycler_listbus.setAdapter(adapter);

        });
        return root;
    }

    private void init() {
        recycler_listbus.setHasFixedSize(true);
        recycler_listbus.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);

        btn_addBus = view.findViewById(R.id.btn_addBus);

        btn_addBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_nav_slideshow_to_nav_add_bus);
            }
        });


    }
}
