package com.example.datvexeadmin.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.datvexeadmin.Model.UserModel;
import com.example.datvexeadmin.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

public class SignupActitity extends AppCompatActivity {

    MaterialEditText phone,username,password,email;
    Button btnSignUp;
    TextView tvLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_actitity);

        username = (MaterialEditText)findViewById(R.id.edtName);
        password = (MaterialEditText)findViewById(R.id.edtPassword);
        phone = (MaterialEditText)findViewById(R.id.edtPhone);
        email = (MaterialEditText)findViewById(R.id.edtEmail);

        btnSignUp = (Button)findViewById(R.id.btnSignUp);
        tvLogin = (TextView) findViewById(R.id.tv_Login);

        //Init Firebase
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("User");

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog mDialog = new ProgressDialog(SignupActitity.this);
                mDialog.setMessage("Please waiting...");
                mDialog.show();

                table_user.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //Check if already user phone
                        if(dataSnapshot.child(phone.getText().toString()).exists())
                        {
                            mDialog.dismiss();
                            Toast.makeText(SignupActitity.this, "Phone Number already register", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            mDialog.dismiss();
                            UserModel user = new UserModel(
                                    username.getText().toString(),
                                    email.getText().toString(),
                                    password.getText().toString(),
                                    phone.getText().toString());

                            table_user.child(phone.getText().toString()).setValue(user);
                            Toast.makeText(SignupActitity.this, "Sign up successfully!", Toast.LENGTH_SHORT).show();
                            goToLoginActivity();
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignupActitity.this,LoginActivity.class));
            }
        });

    }

    private void goToLoginActivity() {
        startActivity(new Intent(SignupActitity.this,LoginActivity.class));
        finish();
    }
}
