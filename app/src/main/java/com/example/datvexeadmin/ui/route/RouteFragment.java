package com.example.datvexeadmin.ui.route;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datvexeadmin.R;
import com.example.datvexeadmin.adapter.ListRouteAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RouteFragment extends Fragment {

    private Unbinder unbinder;

    @BindView(R.id.btn_addRoute)
    Button btn_addRoute;

    private RecyclerView recycler_routeLookup;

    private RouteViewModel routeViewModel;
    NavController navController;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        routeViewModel =
                ViewModelProviders.of(this).get(RouteViewModel.class);
        View root = inflater.inflate(R.layout.fragment_route, container, false);
        recycler_routeLookup = (RecyclerView) root.findViewById(R.id.recycler_routeLookup);
        init();
        unbinder = ButterKnife.bind(this, root);
        routeViewModel.getRouteList().observe(this, listRouteModels -> {
            ListRouteAdapter adapter = new ListRouteAdapter(getContext(), listRouteModels);
            recycler_routeLookup.setAdapter(adapter);
        });

        return root;
    }


    private void init() {
        recycler_routeLookup.setHasFixedSize(true);
        recycler_routeLookup.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
    }

    @Override
    public void onViewCreated(@NonNull View root, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(root, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
