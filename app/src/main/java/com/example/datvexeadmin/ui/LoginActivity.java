package com.example.datvexeadmin.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.datvexeadmin.Common.Common;
import com.example.datvexeadmin.HomeActivity;
import com.example.datvexeadmin.Model.AdminModel;
import com.example.datvexeadmin.Model.UserModel;
import com.example.datvexeadmin.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {


    EditText adminid,password;
    Button btnSignIn;
    private DatabaseReference userRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        password = (MaterialEditText)findViewById(R.id.edtPassword);
        adminid = (MaterialEditText)findViewById(R.id.edtPhone);
        btnSignIn = (Button)findViewById(R.id.btnSignIn);


        //Init Firebase
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("Admin");
        userRef = FirebaseDatabase.getInstance().getReference(Common.ADMIN_REFERENCES);
        btnSignIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final ProgressDialog mDialog = new ProgressDialog(LoginActivity.this);
                mDialog.setMessage("Please waiting...");
                mDialog.show();

                table_user.addValueEventListener(new ValueEventListener() {


                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        //Check if user not exist in database
                        if (dataSnapshot.child(adminid.getText().toString()).exists()) {
                            //Get user information
                            mDialog.dismiss();
                            AdminModel adminModel = dataSnapshot.child(adminid.getText().toString()).getValue(AdminModel.class);
                            if (adminModel.getAdminpass().equals(password.getText().toString())) {
                                userRef.child(adminModel.getAdminid()).setValue(adminModel)
                                        .addOnCompleteListener(task -> {
                                            if (task.isSuccessful()){
                                                Toast.makeText(LoginActivity.this, "Dang nhap thanh cong!", Toast.LENGTH_SHORT).show();
                                                AdminModel adminModel1 = dataSnapshot.getValue(AdminModel.class);
                                                goToHomeActitity(adminModel1);
                                            }
                                        });
                            }
                            else
                            {
                                Toast.makeText(LoginActivity.this, "Wrong Password!!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            mDialog.dismiss();
                            Toast.makeText(LoginActivity.this, "User not exist in Database", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }


//    private void goToSignUpActivity(){
//        startActivity(new Intent(MainActivity.this, SignupActitity.class));
//        finish();
//    }

    private void goToHomeActitity(AdminModel adminModel1) {
        Common.currentAdmin = adminModel1;// Luc nao cung can co du lieu truoc khi su dung
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        finish();
    }
}
