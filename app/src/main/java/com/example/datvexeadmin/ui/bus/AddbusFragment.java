package com.example.datvexeadmin.ui.bus;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.datvexeadmin.Model.ListBusModel;
import com.example.datvexeadmin.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddbusFragment extends Fragment {

    EditText et_busName, et_busSerial, et_busType, et_busSeat, et_busCompany;
    Button btn_addBus;
    NavController navController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_addbus, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference busRef = firebaseDatabase.getReference("Bus");
        navController = Navigation.findNavController(view);

        et_busName = view.findViewById(R.id.et_busName);
        et_busSerial = view.findViewById(R.id.et_busSerial);
        et_busType = view.findViewById(R.id.et_busType);
        et_busSeat = view.findViewById(R.id.et_busSeat);
        et_busCompany = view.findViewById(R.id.et_busCompany);
        btn_addBus = view.findViewById(R.id.btn_addBus);

        btn_addBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog mDialog = new ProgressDialog(getActivity());
                mDialog.setMessage("Đang xử lý...");
                mDialog.show();
                busRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child(et_busSerial.getText().toString()).exists()) {
                            mDialog.dismiss();
                            Toast.makeText(getActivity(), "Mã xe đã tồn tại", Toast.LENGTH_SHORT).show();
                        } else {
                            mDialog.dismiss();
                            ListBusModel busModel = new ListBusModel(
                                    et_busCompany.getText().toString(),
                                    et_busSeat.getText().toString(),
                                    et_busSerial.getText().toString(),
                                    et_busType.getText().toString(),
                                    et_busName.getText().toString());
                            busRef.child(et_busSerial.getText().toString()).setValue(busModel);
                            Toast.makeText(getActivity(), "Đã thêm thành công", Toast.LENGTH_SHORT).show();
                            navController.navigate(R.id.action_nav_add_bus_to_nav_slideshow);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }
}