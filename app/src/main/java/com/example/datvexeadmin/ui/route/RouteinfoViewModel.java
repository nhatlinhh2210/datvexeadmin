package com.example.datvexeadmin.ui.route;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.datvexeadmin.Common.Common;
import com.example.datvexeadmin.Model.ListRouteModel;

public class RouteinfoViewModel extends ViewModel {

    private MutableLiveData<ListRouteModel> mutableLiveDataRoute;

    public RouteinfoViewModel(){
    }

    public MutableLiveData<ListRouteModel> getMutableLiveDataRoute() {
        if (mutableLiveDataRoute == null){
            mutableLiveDataRoute = new MutableLiveData<>();
        }
        mutableLiveDataRoute.setValue(Common.RouteSelected);
        return mutableLiveDataRoute;
    }

    // TODO: Implement the ViewModel
}
