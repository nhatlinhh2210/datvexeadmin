package com.example.datvexeadmin.Common;

import com.example.datvexeadmin.Model.AdminModel;
import com.example.datvexeadmin.Model.ListBusModel;
import com.example.datvexeadmin.Model.ListRouteModel;
import com.example.datvexeadmin.Model.UserModel;

public class Common {
    public static final String USER_REFERENCES = "User";
    public static final String LIST_BUS_REF = "Bus";
    public static final String LIST_ROUTE_REF = "Route";
    public static final String ADMIN_REFERENCES = "Admin";
    public static UserModel currentUser;
    public static AdminModel currentAdmin;
    public static ListBusModel BusSelected;
    public static ListRouteModel RouteSelected;
}
