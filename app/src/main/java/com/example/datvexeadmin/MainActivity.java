package com.example.datvexeadmin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.datvexeadmin.Common.Common;
import com.example.datvexeadmin.Model.UserModel;
import com.example.datvexeadmin.ui.LoginActivity;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.List;

import dmax.dialog.SpotsDialog;
import io.reactivex.disposables.CompositeDisposable;



public class MainActivity extends AppCompatActivity {
        private static int APP_REQUEST_CODE = 7171;
        private FirebaseAuth firebaseAuth;
        private FirebaseAuth.AuthStateListener listener;
        private AlertDialog dialog;
        private CompositeDisposable disposable = new CompositeDisposable();
        private List<AuthUI.IdpConfig> providers;

        private DatabaseReference userRef;

        @Override
        protected void onStart() {
            super.onStart();
            firebaseAuth.addAuthStateListener(listener);
        }

        @Override
        protected void onStop() {
            if (listener != null)
                firebaseAuth.removeAuthStateListener(listener);
            disposable.clear();
            super.onStop();
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            Init();
        }

        private void Init() {
            providers = Arrays.asList(new AuthUI.IdpConfig.PhoneBuilder().build());
            userRef = FirebaseDatabase.getInstance().getReference(Common.USER_REFERENCES);
            firebaseAuth = FirebaseAuth.getInstance();
            dialog = new SpotsDialog.Builder().setCancelable(false).setContext(this).build();
            listener = firebaseAuth -> {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    //Account is already logged in
                    CheckUserFromFirebase(user);
                }
                else {
                    //phoneLogIn();
                    goToLoginActivity();
                }
            };
        }

        private void CheckUserFromFirebase(FirebaseUser user) {
            dialog.show();
            userRef.child(user.getUid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()){
                                Toast.makeText(MainActivity.this, "Đã đăng nhập", Toast.LENGTH_SHORT).show();
                                UserModel userModel = dataSnapshot.getValue(UserModel.class);
                                goToHomeActitity(userModel);
                            }
                            else{
                                goToLoginActivity();
                                //showRegisterDialog(user);
                            }
                            dialog.dismiss();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            dialog.dismiss();
                            Toast.makeText(MainActivity.this, ""+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }

    private void goToLoginActivity() {
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }

    private void showRegisterDialog(FirebaseUser user) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setTitle("Dang ky");
        builder.setMessage("Hay dien day du thong tin");

        View itemView = LayoutInflater.from(this).inflate(R.layout.layout_signup, null);
        EditText et_Username = itemView.findViewById(R.id.et_Username);
        EditText et_Email = itemView.findViewById(R.id.et_Email);
        EditText et_Password = itemView.findViewById(R.id.et_Password);
        EditText et_PhoneNum = itemView.findViewById(R.id.et_PhoneNum);

        // chuyen ve String
        // builder
        builder.setView(itemView);
        builder.setNegativeButton("Huy", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        builder.setPositiveButton("Dang ky", (dialogInterface, i) -> {
           if (TextUtils.isEmpty(et_Username.getText().toString())){
               Toast.makeText(this, "Vui long dien ho ten", Toast.LENGTH_SHORT).show();
               return;
           }
           else if (TextUtils.isEmpty(et_Email.getText().toString())){
               Toast.makeText(this, "Vui long dien dia chi email", Toast.LENGTH_SHORT).show();
               return;
           }
           else if (TextUtils.isEmpty(et_Password.getText().toString())){
                Toast.makeText(this, "Vui long dien mat khau", Toast.LENGTH_SHORT).show();
                return;
           }
           else if (TextUtils.isEmpty(et_PhoneNum.getText().toString())){
               Toast.makeText(this, "Vui long dien so dien thoai", Toast.LENGTH_SHORT).show();
               return;
           }

           //
           UserModel userModel = new UserModel();
           //userModel.setUid(user.getUid());
           userModel.setusername(et_Username.getText().toString());
           userModel.setEmail(et_Email.getText().toString());
           userModel.setPhone(et_PhoneNum.getText().toString());
           userModel.setPassword(et_Password.getText().toString());

           //

        });

        // hien thi bang dang ky
        builder.setView(itemView);
        androidx.appcompat.app.AlertDialog dialog = builder.create();
        dialog.show();


    }


    private void goToHomeActitity(UserModel userModel) {
        Common.currentUser = userModel;// Luc nao cung can co du lieu truoc khi su dung
        startActivity(new Intent(MainActivity.this,HomeActivity.class));
        finish();
    }

    private void phoneLogIn() {
            startActivityForResult(AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers).build(),APP_REQUEST_CODE);
        }
        @Override
        protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == APP_REQUEST_CODE) {
                IdpResponse response = IdpResponse.fromResultIntent(data);
                if (resultCode == RESULT_OK){
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                }
                else {
                    Toast.makeText(this, "Dang nhap that bai", Toast.LENGTH_SHORT).show();
                }
            }
        }
}

