package com.example.datvexeadmin.event;

import com.example.datvexeadmin.Model.ListRouteModel;

public class RouteClick {
    private boolean success;
    private ListRouteModel listRouteModel;

    public RouteClick(boolean success, ListRouteModel listRouteModel) {
        this.success = success;
        this.listRouteModel = listRouteModel;
    }

    public boolean isSuccess(){
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ListRouteModel getListRouteModel() {
        return listRouteModel;
    }

    public void setListRouteModel(ListRouteModel listRouteModel) {
        this.listRouteModel = listRouteModel;
    }
}
