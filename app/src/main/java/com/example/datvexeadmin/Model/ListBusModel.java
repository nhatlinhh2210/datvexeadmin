package com.example.datvexeadmin.Model;

public class ListBusModel {
    private String Company,Serial,Type,Name;
    private String Seat;


    public ListBusModel(String company,String seat, String serial, String type,String name) {
        Company = company;

        Seat = seat;
        Serial = serial;
        Type = type;
        Name = name;
    }


    public void setSeat(String seat) {
        Seat = seat;
    }


    public ListBusModel() {
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getSerial() {
        return Serial;
    }


    public void setSerial(String serial) {
        Serial = serial;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSeat() {
        return Seat;
    }
}


