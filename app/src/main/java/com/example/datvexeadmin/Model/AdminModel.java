package com.example.datvexeadmin.Model;

public class AdminModel {
    String adminid, adminpass;

    public AdminModel() {
    }

    public AdminModel(String adminid, String adminpass) {
        this.adminid = adminid;
        this.adminpass = adminpass;
    }

    public String getAdminid() {
        return adminid;
    }

    public void setAdminid(String adminid) {
        this.adminid = adminid;
    }

    public String getAdminpass() {
        return adminpass;
    }

    public void setAdminpass(String adminpass) {
        this.adminpass = adminpass;
    }
}
