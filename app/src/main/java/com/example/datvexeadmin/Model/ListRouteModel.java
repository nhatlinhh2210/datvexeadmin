package com.example.datvexeadmin.Model;

public class ListRouteModel {
    private String Name, Serial, Start, End, SeatType, TimeStart, TimeEnd, DayStart, DayEnd;
    private String Price,Seat,Distance;

    public ListRouteModel() {
    }

    public ListRouteModel(String name, String serial, String start, String end, String seatType, String timeStart, String timeEnd, String dayStart, String dayEnd, String price, String seat, String distance) {
        Name = name;
        Serial = serial;
        Start = start;
        End = end;
        SeatType = seatType;
        TimeStart = timeStart;
        TimeEnd = timeEnd;
        DayStart = dayStart;
        DayEnd = dayEnd;
        Price = price;
        Seat = seat;
        Distance = distance;
    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSerial() {
        return Serial;
    }

    public void setSerial(String serial) {
        Serial = serial;
    }

    public String getStart() {
        return Start;
    }

    public void setStart(String start) {
        Start = start;
    }

    public String getEnd() {
        return End;
    }

    public void setEnd(String end) {
        End = end;
    }

    public String getSeatType() {
        return SeatType;
    }

    public void setSeatType(String seatType) {
        SeatType = seatType;
    }

    public String getTimeStart() {
        return TimeStart;
    }

    public void setTimeStart(String timeStart) {
        TimeStart = timeStart;
    }

    public String getTimeEnd() {
        return TimeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        TimeEnd = timeEnd;
    }

    public String getDayStart() {
        return DayStart;
    }

    public void setDayStart(String dayStart) {
        DayStart = dayStart;
    }

    public String getDayEnd() {
        return DayEnd;
    }

    public void setDayEnd(String dayEnd) {
        DayEnd = dayEnd;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getSeat() {
        return Seat;
    }

    public void setSeat(String seat) {
        Seat = seat;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }
}
