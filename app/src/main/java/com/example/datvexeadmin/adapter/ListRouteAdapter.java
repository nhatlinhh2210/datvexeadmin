package com.example.datvexeadmin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datvexeadmin.Common.Common;
import com.example.datvexeadmin.Model.ListRouteModel;
import com.example.datvexeadmin.R;
import com.example.datvexeadmin.callback.RecyclerClickListener;
import com.example.datvexeadmin.event.RouteClick;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListRouteAdapter extends RecyclerView.Adapter<ListRouteAdapter.MyViewHolder> {


    List<ListRouteModel> listRouteModels;
    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    DatabaseReference routeRef = firebaseDatabase.getReference("Route");
    Context context;

    public ListRouteAdapter(Context context, List<ListRouteModel> listRouteModels) {
        this.context = context;
        this.listRouteModels = listRouteModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_route_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tv_routeStart.setText(listRouteModels.get(position).getStart());
        holder.tv_routeEnd.setText(listRouteModels.get(position).getEnd());
        holder.tv_routeTimeStart.setText(listRouteModels.get(position).getTimeStart());
        holder.tv_routeTimeEnd.setText(listRouteModels.get(position).getTimeEnd());
//      holder.tv_routePrice.setText(listRouteModels.get(position).getPrice());
        holder.tv_seatNumber.setText(listRouteModels.get(position).getSeat());
        holder.tv_routeDayStart.setText(listRouteModels.get(position).getDayStart());


        //event
//        holder.setClickListener((view, pos) -> {
//            Common.RouteSelected = listRouteModels.get(pos);
//            EventBus.getDefault().postSticky(new RouteClick(true,listRouteModels.get(pos)));
//        });

//        holder.btn_routeDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Common.RouteSelected = listRouteModels.get(position);
//            }
//        });

        holder.setClickListener((view, pos) -> {
            Common.RouteSelected = listRouteModels.get(pos);
            holder.btn_routeDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    routeRef.child(Common.RouteSelected.getSerial()).removeValue();
                    Toast.makeText(context, "Đã xóa tuyến " + Common.RouteSelected.getSerial(), Toast.LENGTH_SHORT).show();
                }
            });
            holder.btn_routeEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().postSticky(new RouteClick(true, Common.RouteSelected));
                }
            });
        });
    }

    @Override
    public int getItemCount() {
        return listRouteModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Unbinder unbinder;

        @BindView(R.id.tv_routeTimeStart)
        TextView tv_routeTimeStart;

        @BindView(R.id.tv_routeTimeEnd)
        TextView tv_routeTimeEnd;

        @BindView(R.id.tv_routeStart)
        TextView tv_routeStart;

        @BindView(R.id.tv_routeEnd)
        TextView tv_routeEnd;

        @BindView(R.id.tv_seatNumber)
        TextView tv_seatNumber;

        @Nullable
        @BindView(R.id.tv_routePrice)
        TextView tv_routePrice;

        @BindView(R.id.tv_routeDayStart)
        TextView tv_routeDayStart;

        @BindView(R.id.btn_routeDelete)
        Button btn_routeDelete;

        @BindView(R.id.btn_routeEdit)
        Button btn_routeEdit;


        RecyclerClickListener clickListener;

        public void setClickListener(RecyclerClickListener clickListener) {
            this.clickListener = clickListener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.OnItemClickListener(view, getAdapterPosition());
        }

    }
}
